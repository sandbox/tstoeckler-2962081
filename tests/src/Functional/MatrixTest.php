<?php

namespace Drupal\Tests\views_matrix\Functional;

use Drupal\Core\Language\LanguageManager;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the matrix style plugin in the browser.
 */
class MatrixTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language', 'views_matrix_test'];

  /**
   * The users created in this test.
   *
   * @var \Drupal\user\UserInterface[]
   */
  protected $users = [];

  /**
   * The languages created in this test.
   *
   * @var \Drupal\language\ConfigurableLanguageInterface[]
   */
  protected $languages = [];

  /**
   * The test entities created in this test.
   *
   * @var \Drupal\entity_test\Entity\EntityTest[]
   */
  protected $testEntities = [];

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');

    $user_storage = $entity_type_manager->getStorage('user');
    for ($i = 0; $i < 10; ++$i) {
      $this->users[$i] = $user_storage->create(['name' => 'Test user ' . $i]);
      $this->users[$i]->save();
    }

    $langcodes = array_keys(LanguageManager::getUnitedNationsLanguageList());
    foreach ($langcodes as $langcode) {
      if ($langcode === 'en') {
        $this->languages[$langcode] = ConfigurableLanguage::load($langcode);
      }
      else {
        $this->languages[$langcode] = ConfigurableLanguage::createFromLangcode($langcode);
        $this->languages[$langcode]->save();
      }
    }

    $test_entity_storage = $entity_type_manager->getStorage('entity_test');
    foreach ($this->users as $user) {
      foreach ($this->languages as $language) {
        $user_id = $user->id();
        $langcode = $language->getId();
        $this->testEntities["$user_id:$langcode"] = $test_entity_storage->create([
          'name' => "Test entity $user_id:$langcode",
          'user_id' => $user_id,
          'langcode' => $langcode,
        ]);
        $this->testEntities["$user_id:$langcode"]->save();
      }
    }
  }

  /**
   * Tests that the test view is correctly displayed in a matrix.
   *
   * @throws \Behat\Mink\Exception\ElementTextException
   */
  public function testMatrix() {
    $this->drupalGet('/matrix-test');

    $header = $this->getSession()->getPage()->findAll('css', 'table thead tr th');
    /* @var \Behat\Mink\Element\NodeElement $empty_cell */
    $empty_cell = array_shift($header);
    $this->assertSame('', $empty_cell->getText());
    $this->assertCount(count($this->languages), $header);
    foreach ($this->languages as $language) {
      /* @var \Behat\Mink\Element\NodeElement $heading */
      $heading = array_shift($header);
      $this->assertSame($language->label(), $heading->getText());
    }

    $first_column = $this->getSession()->getPage()->findAll('css', 'table tbody tr td a');
    $this->assertCount(count($this->users), $first_column);
    foreach ($this->users as $user) {
      /* @var \Behat\Mink\Element\NodeElement $first_column_cell */
      $first_column_cell = array_shift($first_column);
      $this->assertSame($user->getDisplayName(), $first_column_cell->getText());
    }

    $entries = $this->getSession()->getPage()->findAll('css', 'table tbody tr td div span');
    $this->assertCount(count($this->testEntities), $entries);
    foreach ($this->testEntities as $test_entity) {
      /* @var \Behat\Mink\Element\NodeElement $entry */
      $entry = array_shift($entries);
      $this->assertSame($test_entity->label(), $entry->getText());
    }
  }

}
