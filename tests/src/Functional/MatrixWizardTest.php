<?php

namespace Drupal\Tests\views_matrix\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\views\Entity\View;
use Drupal\views_matrix\Plugin\views\style\Matrix;

/**
 * Tests the matrix style plugin in the Views wizard in the browser.
 */
class MatrixWizardTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['entity_test', 'views_matrix', 'views_ui'];

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $viewStorage;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function setUp() {
    parent::setUp();

    /* @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $this->viewStorage = $entity_type_manager->getStorage('view');
  }

  /**
   * Tests selecting the Matrix style plugin in the standard Views wizard.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testViewsWizard() {
    $admin = $this->drupalCreateUser(['administer views']);
    $this->drupalLogin($admin);

    $view_id = 'matrix';
    $this->drupalGet('admin/structure/views/add');
    $page = $this->getSession()->getPage();
    $page->fillField('View name', 'Matrix');
    $page->fillField('Machine-readable name', $view_id);
    $page->selectFieldOption('Show', 'Test entity');
    $page->checkField('Create a page');
    $page->fillField('Path', '/matrix');
    $page->selectFieldOption('Display format', 'Matrix');
    $page->pressButton('Save and edit');

    /* @var \Drupal\views\Entity\View $view */
    $view = $this->viewStorage->load($view_id);
    $this->assertInstanceOf(View::class, $view);
    $executable = $view->getExecutable();
    $executable->initDisplay();
    $style = $executable->getStyle();
    $this->assertInstanceOf(Matrix::class, $style);
    $dependencies = $view->getDependencies();
    $this->assertArrayHasKey('module', $dependencies);
    $this->assertContains('views_matrix', $dependencies['module']);

    $this->assertSession()->addressEquals('/admin/structure/views/view/' . $view_id);
    $page = $this->getSession()->getPage();
    // Find the "Settings" link for the "Format" row by its title, because there
    // are multiple "Settings" links on this page.
    $page->clickLink('Change settings for this format');
    $page = $this->getSession()->getPage();
    $page->pressButton('Apply');

    $options = [
      'grouping' => [
        ['field' => 'name'],
        ['field' => 'name'],
      ],
    ];
    $this->assertArraySubset($options, $style->options);
    /* @see \Drupal\views\Plugin\views\wizard\WizardPluginBase::addDisplays() */
    $this->assertSession()->addressEquals('/admin/structure/views/view/' . $view_id . '/edit/page_1');
  }

}
