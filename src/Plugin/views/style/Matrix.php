<?php

namespace Drupal\views_matrix\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\Plugin\views\wizard\WizardInterface;
use Drupal\views\ResultRow;

/**
 * Style plugin to render each item as a cell in a table.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "matrix",
 *   title = @Translation("Matrix"),
 *   help = @Translation("Displays cells identified by row and column in a table."),
 *   theme = "views_view_matrix_row",
 *   display_types = {"normal"},
 * )
 */
class Matrix extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = parent::render();
    if ($rows) {
      $count = count($rows);

      // @todo Allow for table attributes, caption, etc.
      $rows[0]['#prefix'] = '<table>';
      $rows[$count - 1]['#suffix'] = '</table>';
      if ($count > 1) {
        $rows[0]['#prefix'] .= '<thead>';
        $rows[0]['#suffix'] = '</thead>';
        $rows[1]['#prefix'] = '<tbody>';
        $rows[$count - 1]['#suffix'] = '</tbody>' . $rows[$count - 1]['#suffix'];
      }
    }
    return $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function renderGroupingSets($sets) {
    $output = [];
    if (key($sets) === '') {
      $set = array_shift($sets);
      $level = isset($set['level']) ? $set['level'] : 0;
      $output[] = [
        '#theme' => $this->view->buildThemeFunctions('views_view_matrix_header_row'),
        '#view' => $this->view,
        '#rows' => $set['rows'],
        '#grouping_level' => $level,
        '#title' => $set['group'],
      ];
    }
    $output = array_merge($output, parent::renderGroupingSets($sets));
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function renderGrouping($records, $groupings = [], $group_rendered = NULL) {
    // This is for backward compatibility, when $groupings was a string
    // containing the ID of a single field.
    if (is_string($groupings)) {
      $rendered = $group_rendered === NULL ? TRUE : $group_rendered;
      $groupings = [['field' => $groupings, 'rendered' => $rendered]];
    }

    // Make sure fields are rendered.
    $this->renderFields($this->view->result);
    if (count($groupings) === 2) {
      $sets = [];
      $sets[''] = [
        'group' => '',
        'level' => 0,
        'rows' => [
        ],
      ];
      foreach ($records as $row_index => $row) {
        list($matrix_row, $matrix_row_content) = $this->getGroupContent($row_index, $groupings[0], $group_rendered);
        if (empty($sets[$matrix_row])) {
          $sets[$matrix_row]['group'] = $matrix_row_content;
          $sets[$matrix_row]['level'] = 0;
          $sets[$matrix_row]['rows'] = [];
          foreach ($sets['']['rows'] as $header_index => $header) {
            if ($header_index !== '') {
              $sets[$matrix_row]['rows'][$header_index] = new ResultRow();
            }
          }
        }
        list($matrix_column, $matrix_column_content) = $this->getGroupContent($row_index, $groupings[1], $group_rendered);
        if (empty($sets['']['rows'][$matrix_column])) {
          reset($sets);
          foreach ($sets as $set_index => &$set) {
            if ($set_index === '') {
              // See views-view-matrix-header-row.html.twig.
              $set['rows'][$matrix_column] = $matrix_column_content;
            }
            else {
              $set['rows'][$matrix_column] = new ResultRow();
            }
          }
        }

        // Add the row to the hierarchically positioned row set we just
        // determined.
        $sets[$matrix_row]['rows'][$matrix_column] = $row;
      }
    }
    else {
      $sets = parent::renderGrouping($records, $groupings, $group_rendered);
    }

    // If this parameter isn't explicitly set, modify the output to be fully
    // backward compatible to code before Views 7.x-3.0-rc2.
    // @TODO Remove this as soon as possible e.g. October 2020
    if ($group_rendered === NULL) {
      $old_style_sets = [];
      foreach ($sets as $group) {
        $old_style_sets[$group['group']] = $group['rows'];
      }
      $sets = $old_style_sets;
    }

    return $sets;
  }

  /**
   * Gets the content to group a result by.
   *
   * @param int $index
   *   The index of the row.
   * @param array $info
   *   The grouping information containing the following keys:
   *   - field: The field to group by.
   *   - rendered: Whether or not to use the rendered output for grouping.
   *   - renderd_strip: If rendered output is to be used for grouping whether
   *     HTML tags should be stripped from the output.
   * @param bool $group_rendered
   *   Whether or not to use the rendered output for grouping by default.
   *
   * @return array
   *   An array containing the grouping key as the first value and the grouping
   *   content as the second value.
   */
  protected function getGroupContent($index, array $info, $group_rendered) {
    $field = $info['field'];
    $rendered = isset($info['rendered']) ? $info['rendered'] : $group_rendered;
    $rendered_strip = isset($info['rendered_strip']) ? $info['rendered_strip'] : FALSE;
    $grouping = '';
    $group_content = '';
    // Group on the rendered version of the field, not the raw.  That way,
    // we can control any special formatting of the grouping field through
    // the admin or theme layer or anywhere else we'd like.
    if (isset($this->view->field[$field])) {
      $group_content = $this->getField($index, $field);
      if ($this->view->field[$field]->options['label']) {
        $group_content = $this->view->field[$field]->options['label'] . ': ' . $group_content;
      }
      if ($rendered) {
        $grouping = (string) $group_content;
        if ($rendered_strip) {
          $group_content = $grouping = strip_tags(htmlspecialchars_decode($group_content));
        }
      }
      else {
        $grouping = $this->getFieldValue($index, $field);
        // Not all field handlers return a scalar value,
        // e.g. views_handler_field_field.
        if (!is_scalar($grouping)) {
          $grouping = hash('sha256', serialize($grouping));
        }
      }
    }
    return [$grouping, $group_content];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    $field_labels = $this->displayHandler->getFieldLabels(TRUE);
    // Show a warning if there are no fields.
    if (!$field_labels) {
      $errors[] = $this->t('Style @style requires fields to group by but there are none defined for this display.', ['@style' => $this->definition['title']]);
    }
    elseif (empty($this->options['grouping'][0]['field']) || empty($this->options['grouping'][1]['field'])) {
      $errors[] = $this->t('Style @style requires a row and column grouping to be configured.', ['@style' => $this->definition['title']]);
    }
    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $field_labels = $this->displayHandler->getFieldLabels(TRUE);
    // Show a warning if there are now fields.
    if (!$field_labels) {
      drupal_set_message($this->t('This style requires fields to group by but there are none defined for this display.'), 'error');
    }

    // This is for backward compatibility, when there was just a single
    // select form.
    if (is_string($this->options['grouping'])) {
      $grouping = $this->options['grouping'];
      $this->options['grouping'] = [];
      $this->options['grouping'][0]['field'] = $grouping;
    }
    if (isset($this->options['group_rendered']) && is_string($this->options['group_rendered'])) {
      $this->options['grouping'][0]['rendered'] = $this->options['group_rendered'];
      unset($this->options['group_rendered']);
    }

    // If no grouping is configured yet, add an empty grouping so that at least
    // two levels of grouping are always shown.
    if (!$this->options['grouping']) {
      $this->options['grouping'][] = [
        'field' => '',
        'rendered' => TRUE,
        'rendered_strip' => FALSE,
      ];
    }
    parent::buildOptionsForm($form, $form_state);
    // If there are no fields, we can't group on them.
    if ($this->usesFields() && $this->usesGrouping() && ($field_labels > 1)) {
      $form['grouping'] = array_slice($form['grouping'], 0, 2);

      $form['grouping'][0]['field']['#title'] = $this->t('Row grouping field');
      $form['grouping'][0]['field']['#description'] = $this->t('A field by which to group the records into rows. The output of this field will form the first column of the table.');
      $form['grouping'][0]['field']['#required'] = TRUE;

      $form['grouping'][1]['field']['#title'] = $this->t('Column grouping field');
      $form['grouping'][1]['field']['#description'] = $this->t('A field by which to group the records into columns. The output of this field will form the header row of the table.');
      $form['grouping'][1]['field']['#required'] = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function wizardSubmit(&$form, FormStateInterface $form_state, WizardInterface $wizard, &$display_options, $display_type) {
    $fields = [];
    if (!empty($display_options[$display_type]['fields'])) {
      $fields = array_keys($display_options[$display_type]['fields']);
    }
    elseif (!empty($display_options['default']['fields'])) {
      $fields = array_keys($display_options['default']['fields']);
    }

    if ($fields) {
      $row_grouping = array_shift($fields);
      $column_grouping = $fields ? array_shift($fields) : $row_grouping;

      $display_options[$display_type]['style']['options'] = [
        'grouping' => [
          0 => [
            'field' => $row_grouping,
            'rendered' => TRUE,
            'rendered_strip' => FALSE,
          ],
          1 => [
            'field' => $column_grouping,
            'rendered' => TRUE,
            'rendered_strip' => FALSE,
          ],
        ],
      ];
    }
  }

}
